package eltex.bulat.Task2;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

public class ShoppingCart extends ArrayList<AbstractProduct> {
    private HashSet<UUID> uuidSet = new HashSet<UUID>();

    public HashSet<UUID> getUuidSet() {
        return uuidSet;
    }

    @Override
    public boolean add(AbstractProduct product){
        uuidSet.add(product.getObjectId());
        return super.add(product);

    }
    public void read(){
        System.out.println(this);
    }
    public AbstractProduct search(UUID id){
        for (AbstractProduct product : this){
            if (product.getObjectId().equals(id)){
                return product;
            }
        }
        return null;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (AbstractProduct product : this){
            sb.append(product);
        }
        return sb.toString();
    }
}
