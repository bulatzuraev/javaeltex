package eltex.bulat.Task2;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Orders extends LinkedList<Order> {

    void makePurchase(ShoppingCart shoppingCart, Credential credential, int awaitingTime){
        this.add(new Order(Status.awaiting, awaitingTime, shoppingCart, credential));
    }
    void checkUp(){
        Iterator<Order> it = this.iterator();
        while (it.hasNext()){
            Order order = it.next();
            Instant awaiting = order.getCreationTime().plus(order.getAwaitingTime());
            if (order.getStatus() == Status.processed || Instant.now().compareTo(awaiting) > 0){
                it.remove();
            }
        }
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (Order order : this){
            sb.append(order);
        }
        return sb.toString();
    }
}
