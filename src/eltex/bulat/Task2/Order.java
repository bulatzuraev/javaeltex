package eltex.bulat.Task2;

import java.util.ArrayList;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;

enum Status{
    awaiting,
    processed;
}

public class Order {
    private static HashMap<Instant, Order> orderMap = new HashMap<Instant, Order>();
    private Status status;
    private Instant creationTime;
    private Duration awaitingTime;
    ShoppingCart shoppingCart;
    Credential credential;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public Duration getAwaitingTime() {
        return awaitingTime;
    }

    public void setAwaitingTime(Duration awaitingTime) {
        this.awaitingTime = awaitingTime;
    }

    public static HashMap<Instant, Order> getOrderMap() {
        return orderMap;
    }

    Order(Status status, int awaitingTime, ShoppingCart shoppingCart, Credential credential){
        this.status = status;
        this.creationTime = Instant.now();
        this.awaitingTime = Duration.ofSeconds(awaitingTime);
        this.shoppingCart = shoppingCart;
        this.credential = credential;
        orderMap.put(creationTime, this);
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Status: ");
        sb.append(status);
        sb.append(System.getProperty("line.separator"));
        sb.append("Creation time: ");
        sb.append(creationTime);
        sb.append(System.getProperty("line.separator"));
        sb.append("Awaiting time: ");
        sb.append(awaitingTime);
        sb.append(System.getProperty("line.separator"));
        return sb.toString() + shoppingCart.toString() + credential.toString();
    }
}
