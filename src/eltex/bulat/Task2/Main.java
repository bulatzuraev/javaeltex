package eltex.bulat.Task2;

import java.util.Iterator;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int number;
        Orders orders = new Orders();
        Credential cred = new Credential("1", "Ivan", "Ivanov", "Ivanovich");
        try{
            number = Integer.parseInt(args[0]);
        } catch(NumberFormatException e){
            System.out.println("first arg is not integer, terminate");
            return;
        } catch(ArrayIndexOutOfBoundsException e){
            System.out.println("no args");
            return;
        }
        ShoppingCart cart = new ShoppingCart();
        switch(args[1]){
            case "Coffee":
                for (int i = 0; i < number; ++i){
                    Coffee c1 = new Coffee();
                    c1.update();
                    c1.read();
                    cart.add(c1);
                }
                break;
            case "Tea":
                for (int i = 0; i < number; ++i){
                    Tea t1 = new Tea();
                    t1.update();
                    t1.read();
                }
                break;
        }
        Coffee c2 = new Coffee();
        Coffee c3 = new Coffee();
        ShoppingCart cart1 = new ShoppingCart();
        cart1.add(c2);
        cart1.add(c3);
        orders.makePurchase(cart, cred, 4);
        orders.makePurchase(cart1, cred, 6);
        orders.read();
        while (!orders.isEmpty()){
            orders.checkUp();
        }
        orders.read();
        Iterator<UUID> it = cart.getUuidSet().iterator();
        System.out.println(cart.search(it.next()));
    }
}
