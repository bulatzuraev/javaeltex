package eltex.bulat.Task2;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
