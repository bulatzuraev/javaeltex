package eltex.bulat.Task6.product;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
