package eltex.bulat.Task6.check;

import eltex.bulat.Task6.orders.Order;
import eltex.bulat.Task6.orders.Orders;
import eltex.bulat.Task6.orders.Status;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class AwaitingCheck extends ACheck {
    public AwaitingCheck(Orders orders){
        super(orders);
    }
    public void check(){
        synchronized (orders){
            Iterator<Order> it = orders.iterator();
            while (it.hasNext()){
                Order order = it.next();
                if (order.getStatus() == Status.awaiting){
                    order.changeStatus();
                }
            }
        }
    }
    public void run(){
        while (true){
            check();
        }

    }
}
