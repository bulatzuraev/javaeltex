package eltex.bulat.Task6.check;

import eltex.bulat.Task6.orders.Order;
import eltex.bulat.Task6.orders.Orders;
import eltex.bulat.Task6.orders.Status;

import java.time.Instant;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ProcessedCheck extends ACheck {
    IReporter reporter;
    public ProcessedCheck(Orders orders, IReporter reporter){
        super(orders);
        this.reporter = reporter;
    }
    public void check(){
        synchronized (orders){
            //System.out.println("check");
            Iterator<Order> it = orders.iterator();
            while (it.hasNext()){
                Order order = it.next();
                Instant awaiting = order.getCreationTime().plus(order.getAwaitingTime());
                if (order.getStatus() == Status.processed || Instant.now().compareTo(awaiting) > 0){
                    it.remove();
                    reporter.report(order.getId(), order.getStatus());
                }
            }
        }
    }
    public void run(){
        while (true){
            check();
        }

    }
}
