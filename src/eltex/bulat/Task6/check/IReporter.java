package eltex.bulat.Task6.check;

import eltex.bulat.Task6.orders.Status;

import java.util.UUID;

public interface IReporter {
    void report(UUID id, Status status);
}
