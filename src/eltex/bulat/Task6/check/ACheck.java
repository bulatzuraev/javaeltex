package eltex.bulat.Task6.check;

import eltex.bulat.Task6.orders.Orders;

public abstract class ACheck implements Runnable{
    Orders orders;
    protected ACheck(Orders orders){
        this.orders = orders;
    }
    public abstract void check();
}
