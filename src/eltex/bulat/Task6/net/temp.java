/*
package eltex.bulat.Task6.net;

import eltex.bulat.Task6.check.*;
import eltex.bulat.Task6.orders.Order;
import eltex.bulat.Task6.orders.Orders;
import eltex.bulat.Task6.orders.Status;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server{
    Orders orders = new Orders();

    int serverPortUDPAddress = 1231;
    int serverPortUDPStatus = 1232;
    int clientPortUDPAddress = 1233;
    int clientPortUDPStatus = 1234;
    HashMap<UUID, InetAddress> map = new HashMap<>();
    public void run(){
        Thread thread1 = new Thread(new DataSend());
        Thread thread2 = new Thread(new Listen());
        Thread thread3 = new Thread(new AwaitingCheck(orders));
        Thread thread4 = new Thread(new ProcessedCheck(orders, new Reporter()));
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }
    public static void main(String[] args){
        Server server = new Server();
        server.run();

    }
    class DataSend implements Runnable{

        public void run(){
            try(DatagramSocket socket = new DatagramSocket(serverPortUDPAddress)){
                socket.setBroadcast(true);
                while (true){
                    DatagramPacket packet =
                            new DatagramPacket(new byte[0],0,InetAddress.getByName("255.255.255.255"),clientPortUDPAddress);
                    socket.send(packet);
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    class Listen implements Runnable {
        public void run(){
            Order order = null;
            InetAddress address= null;
            try(ServerSocket serverSocket = new ServerSocket(serverPortUDPAddress)){

            } catch (IOException){

            }
            while (true) {
                try(
                        ServerSocket serverSocket = new ServerSocket(serverPortUDPAddress);
                        Socket clientSocket = serverSocket.accept();
                        ObjectInputStream in = (new ObjectInputStream(clientSocket.getInputStream()))
                ){
                    Thread thread = new Thread(new Runnable(){
                        public void run(){
                            try(){

                            }
                        }
                    });
                    address = clientSocket.getInetAddress();
                    System.out.println(clientSocket.getRemoteSocketAddress());
                    order = (Order)in.readObject();
                    synchronized (orders){
                        orders.makePurchase(order);
                        //synchronized (map){
                        map.put(order.getId(), address);
                        //}
                    }
                } catch(IOException e){
                    e.printStackTrace();
                } catch(ClassNotFoundException e){
                    e.printStackTrace();
                }
            }
        }
    }
    class Reporter implements IReporter{
        InetAddress address = null;
        public void report(UUID id, Status status){
            try(DatagramSocket socket = new DatagramSocket(serverPortUDPStatus)){
                address = map.get(id);
                byte[] data = status.toString().getBytes();
                DatagramPacket packet =
                        new DatagramPacket(data, data.length,address,clientPortUDPStatus);
                socket.send(packet);
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}

*/
