package eltex.bulat.Task6.net;

import eltex.bulat.Task6.check.OrdersGenerator;
import eltex.bulat.Task6.orders.Order;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class Client{
    int serverPort;
    InetAddress serverAdress;
    int udpListenPortAddress = 1233;
    int udpListenPortStatus = 1234;
    public void run(){
        waitAddress();
        while (true){
            Order order = sendOrder();
            System.out.println(Duration.between(order.getCreationTime(), Instant.now()));
        }
    }
    public static void main(String[] args){
        Client client = new Client();
        client.run();

    }
    private void waitAddress(){
        try(
                DatagramSocket socket = new DatagramSocket(udpListenPortAddress);
        ){
            DatagramPacket packet = new DatagramPacket(new byte[10],10);
            socket.receive(packet);
            serverAdress = packet.getAddress();
            serverPort = Integer.valueOf(new String(packet.getData(),0,packet.getLength()));
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    private Order sendOrder(){
        Order order = null;
        try(
                Socket clientSocket = new Socket(serverAdress,serverPort);
                DatagramSocket socket = new DatagramSocket();
                ObjectOutputStream out = (new ObjectOutputStream(clientSocket.getOutputStream()))
        ){
            DatagramPacket packet = new DatagramPacket(new byte[10],10);
            order = OrdersGenerator.generateOrder();
            out.writeObject(order);
            out.writeObject(socket.getLocalPort());
            socket.receive(packet);
            System.out.println(new String(packet.getData(),0,packet.getLength()));
        } catch (IOException e){
            e.printStackTrace();
        }
        return order;
    }
    /*private void waitStatus(){
        try(
                DatagramSocket socket = new DatagramSocket(udpListenPortStatus);
        ){
            DatagramPacket packet = new DatagramPacket(new byte[10],10);
            socket.receive(packet);
            System.out.println(new String(packet.getData(),0,packet.getLength()));
        } catch (IOException e){
            e.printStackTrace();
        }
    }*/

}
