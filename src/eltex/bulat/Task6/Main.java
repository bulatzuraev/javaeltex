package eltex.bulat.Task6;

import eltex.bulat.Task6.check.OrdersGenerator;
import eltex.bulat.Task6.manager.ManagerOrderFile;
import eltex.bulat.Task6.manager.ManagerOrderJSON;
import eltex.bulat.Task6.orders.Orders;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        Orders orders = new Orders();
        Thread genThread1 = new Thread(new OrdersGenerator(orders,1));
        genThread1.start();
        try{
            TimeUnit.SECONDS.sleep(1);
            genThread1.interrupt();
        } catch (InterruptedException e){

        }
        ManagerOrderFile managerFile = new ManagerOrderFile(orders, "save.file");
        ManagerOrderJSON managerJSON = new ManagerOrderJSON(orders, "save.json");
        managerJSON.saveAll();
        managerJSON.readAll();;
        System.out.println(orders);
    }
}
