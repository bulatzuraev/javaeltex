package eltex.bulat.Task4;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ProcessedCheck extends ACheck {
    public ProcessedCheck(Orders orders){
        super(orders);
    }
    public void check(){
        synchronized (orders){
            orders.checkUp();
            System.out.println(orders.size());
        }
    }
    public void run(){
        while (true){
            try {
                check();
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e){
                System.err.println("Interrupted");
            }
        }

    }
}
