package eltex.bulat.Task4;

public abstract class ACheck implements Runnable{
    Orders orders;
    protected ACheck(Orders orders){
        this.orders = orders;
    }
    public abstract void check();
}
