package eltex.bulat.Task4;

import java.util.Iterator;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        Orders orders = new Orders();
        Thread genThread1 = new Thread(new OrdersGenerator(orders));
        Thread checkThread1 = new Thread(new AwaitingCheck(orders));
        Thread checkThread2 = new Thread(new ProcessedCheck(orders));
        genThread1.start();
        checkThread1.start();
        checkThread2.start();
        for (int i = 1; i < 5; ++i){
            Thread genThread = new Thread(new OrdersGenerator(orders,i));
            genThread.start();
        }
    }
}
