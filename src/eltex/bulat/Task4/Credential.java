package eltex.bulat.Task4;

import java.util.UUID;

public class Credential {
    private UUID id;
    private String firstName;
    private String secondName;
    private String middleName;

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Credential(String firstName, String secondName, String middleName) {
        id = UUID.randomUUID();
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(id);
        sb.append(System.getProperty("line.separator"));
        sb.append("First name: ");
        sb.append(firstName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Second name: ");
        sb.append(secondName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Middle name: ");
        sb.append(middleName);
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}
