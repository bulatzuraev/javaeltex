package eltex.bulat.Task4;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OrdersGenerator implements Runnable {
    Orders orders;
    int sleepTime;
    OrdersGenerator(Orders orders){
        this.orders = orders;
        sleepTime = 2;
    }
    public OrdersGenerator(Orders orders, int sleepTime){
        this.orders = orders;
        this.sleepTime = sleepTime;
    }
    private AbstractProduct generateProduct(String product){
        if (product == "Tea"){
            return new Tea();
        } else if (product == "Coffee"){
            return new Coffee();
        }
        return null;
    }
    private ShoppingCart generateShoppingCart(){
        String[] productType = {"Tea","Coffee"};
        ShoppingCart cart = new ShoppingCart();
        Random rand = new Random();
        for (int i = 0; i < 5; ++i){
            cart.add(generateProduct(productType[rand.nextInt(1)]));
        }
        return cart;
    }
    private Credential generateCredential(){
        Random rand = new Random();
        int i = rand.nextInt(100);
        String firstName = "firstName" + i;
        String secondName = "firstName" + i;
        String middleName = "firstName" + i;
        return new Credential(firstName, secondName, middleName);
    }
    public void run(){
        Random rand = new Random();
        while (true){
            try {
                synchronized (orders){
                    orders.makePurchase(new Order(rand.nextInt(10),generateShoppingCart(),generateCredential()));
                    System.out.println("makePurchase, sleeptime="+sleepTime);
                }
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e){
                System.err.println("Interrupted");
            }

        }
    }
}
