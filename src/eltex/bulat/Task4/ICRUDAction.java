package eltex.bulat.Task4;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
