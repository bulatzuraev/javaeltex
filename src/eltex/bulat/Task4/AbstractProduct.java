package eltex.bulat.Task4;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public abstract class AbstractProduct implements ICRUDAction {

    protected static int productCounter;
    private UUID objectId;
    private String name;
    private int cost;
    private String firm;
    private String country;
    protected AbstractProduct(){
        ++productCounter;
        this.objectId = UUID.randomUUID();
    }

    protected static int getProductCounter() {
        return productCounter;
    }

    protected UUID getObjectId() {
        return objectId;
    }

    protected void setObjectId(UUID objectId) {
        this.objectId = objectId;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected int getCost() {
        return cost;
    }

    protected void setCost(int cost) {
        this.cost = cost;
    }

    protected String getFirm() {
        return firm;
    }

    protected void setFirm(String firm) {
        this.firm = firm;
    }

    protected String getCountry() {
        return country;
    }

    protected void setCountry(String country) {
        this.country = country;
    }
    public void create(){
        Random rand = new Random();
        int i = rand.nextInt(100);
        setName("name" + i);
        setCost(rand.nextInt(100));
        i = rand.nextInt(100);
        setFirm("firm" + i);
        i = rand.nextInt(100);
        setCountry("country" + i);
    }
    public void read(){
        System.out.println(this);
    }
    public void update() throws NumberFormatException{
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter name");
            setName(scan.nextLine());
            System.out.println("Enter cost");
            setCost(Integer.parseInt(scan.nextLine()));
            System.out.println("Enter firm");
            setFirm(scan.nextLine());
            System.out.println("Enter country");
            setCountry(scan.nextLine());
    }
    public void delete(){
        setObjectId(null);
        setName(null);
        setCost(0);
        setFirm(null);
        setCountry(null);
        --productCounter;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Product counter: ");
        sb.append(productCounter);
        sb.append(System.getProperty("line.separator"));
        sb.append("Object ID: ");
        sb.append(objectId);
        sb.append(System.getProperty("line.separator"));
        sb.append("Name: ");
        sb.append(name);
        sb.append(System.getProperty("line.separator"));
        sb.append("Cost: ");
        sb.append(cost);
        sb.append(System.getProperty("line.separator"));
        sb.append("Firm: ");
        sb.append(firm);
        sb.append(System.getProperty("line.separator"));
        sb.append("Country: ");
        sb.append(country);
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}