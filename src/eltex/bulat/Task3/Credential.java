package eltex.bulat.Task3;

public class Credential {
    private String id;
    private String firstName;
    private String secondName;
    private String middleName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Credential(String id, String firstName, String secondName, String middleName) {

        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(id);
        sb.append(System.getProperty("line.separator"));
        sb.append("First name: ");
        sb.append(firstName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Second name: ");
        sb.append(secondName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Middle name: ");
        sb.append(middleName);
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}
