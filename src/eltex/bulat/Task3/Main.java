package eltex.bulat.Task3;

import java.util.Iterator;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        Orders<Order> orders = new Orders();
        Credential cred = new Credential("1", "Ivan", "Ivanov", "Ivanovich");
        ShoppingCart<Tea> cartTea = new ShoppingCart();
        cartTea.add(new Tea());
        ShoppingCart<Coffee> cartCoffee = new ShoppingCart();
        cartCoffee.add(new Coffee());
        cartCoffee.add(new Coffee());
        orders.makePurchase(new Order(4, cartCoffee, cred));
        orders.makePurchase(new Order(6, cartTea, cred));
        orders.read();
        while (!orders.isEmpty()){
            orders.checkUp();
        }
        orders.read();
        Iterator<UUID> it = cartTea.getUuidSet().iterator();
        System.out.println(cartTea.search(it.next()));
    }
}
