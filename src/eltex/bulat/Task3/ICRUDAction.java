package eltex.bulat.Task3;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
