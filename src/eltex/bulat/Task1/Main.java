package eltex.bulat.Task1;

import eltex.bulat.Task1.Coffee;
import eltex.bulat.Task1.Tea;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int number;
        try{
            number = Integer.parseInt(args[0]);
        } catch(NumberFormatException e){
            System.out.println("first arg is not integer, terminate");
            return;
        }
        switch(args[1]){
            case "Coffee":
                for (int i = 0; i < number; ++i){
                    Coffee c1 = new Coffee();
                    c1.update();
                    c1.read();
                }
                break;
            case "Tea":
                for (int i = 0; i < number; ++i){
                    Tea t1 = new Tea();
                    t1.update();
                    t1.read();
                }
                break;
        }
    }
}
