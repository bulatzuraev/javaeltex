package eltex.bulat.Task1;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
