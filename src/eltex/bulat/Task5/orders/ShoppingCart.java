package eltex.bulat.Task5.orders;



import eltex.bulat.Task5.product.AbstractProduct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

public class ShoppingCart <T extends AbstractProduct> extends ArrayList<T> implements Serializable {
    private HashSet<UUID> uuidSet = new HashSet<UUID>();

    public HashSet<UUID> getUuidSet() {
        return uuidSet;
    }

    @Override
    public boolean add(T product){
        uuidSet.add(product.getObjectId());
        return super.add(product);

    }
    public void read(){
        System.out.println(this);
    }
    public T search(UUID id){
        for (T product : this){
            if (product.getObjectId().equals(id)){
                return product;
            }
        }
        return null;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (T product : this){
            sb.append(product);
        }
        return sb.toString();
    }
}
