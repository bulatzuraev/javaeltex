package eltex.bulat.Task5.product;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
