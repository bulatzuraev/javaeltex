package eltex.bulat.Task5.check;

import eltex.bulat.Task5.orders.*;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class AwaitingCheck extends ACheck {
    public AwaitingCheck(Orders orders){
        super(orders);
    }
    public void check(){
        synchronized (orders){
            Iterator<Order> it = orders.iterator();
            while (it.hasNext()){
                Order order = it.next();
                if (order.getStatus() == Status.awaiting){
                    order.changeStatus();
                }
            }
        }
    }
    public void run(){
        while (true){
            try {
                check();
                System.out.println("awaiting");
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e){
                System.err.println("Interrupted");
            }
        }

    }
}
