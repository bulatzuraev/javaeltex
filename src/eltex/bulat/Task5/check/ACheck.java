package eltex.bulat.Task5.check;

import eltex.bulat.Task5.orders.Orders;

public abstract class ACheck implements Runnable{
    Orders orders;
    protected ACheck(Orders orders){
        this.orders = orders;
    }
    public abstract void check();
}
